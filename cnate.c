#include <stdio.h>
#include <stdlib.h>

// Build the array
int *getArrayFromUser(int muxCount)
{
    // Loop over muxCount, and build the array
    int *snitch = malloc(muxCount);
    for (int i = 0; i < muxCount; i++) {
        int GotIt = 0;
        int Macguffin;
        char buffer[3];
        while (GotIt == 0) {
            printf("Entry number #%i: ", i+1);
            fgets(buffer,100,stdin);
            Macguffin = (int)strtol(buffer,NULL,10);
            if(Macguffin > 99 || Macguffin < -99 || Macguffin == 0){ // Don't allow 0 because non-integers return 0
                GotIt = 0;
                printf("Sorry, that's not a valid integer for this matrix. Try again!\n");
            } else {
                snitch[i] = Macguffin;
                GotIt = 1;
            }
        }
    }
    return snitch;
}

void muxArray(int *muxTarget, int muxCount) {
    int FinalForm[muxCount];
    for (int i = 0; i < muxCount; i++) {
        int Stepping = 1;
        // Do the magicks
        for (int x = 0; x < muxCount; x++) {
            if (x!=i) {
                Stepping = Stepping * muxTarget[x];
            }
        }
        FinalForm[i] = Stepping;
    }
    // Dump final tally
    printf("\nFinal tally: [");
    for (int i = 0; i < muxCount; i++) {
        printf("%i", FinalForm[i]);
        if (i < muxCount - 1) { printf(", ");}
    }
    printf("]\n");
}

// Main Logic
int main()
{
    printf("This programme will compute the products of an array of integers, cycling through while ignoring the current index.\n");
    int muxCount = 0;
    int GotIt = 0;
    char buffer[3];
    while (GotIt == 0) {
        printf("How many integers would you like to compute? ");
        fgets(buffer,100,stdin);
        muxCount = (int)strtol(buffer,NULL,10); 
        if (muxCount > 10 || muxCount < 3){
            printf("Sorry, that is not a valid number of array members. Try again!\n");
            GotIt = 0;
        } else {
            GotIt = 1;
            printf("Right, we'll use %i integers for the calculation matrix.\n", muxCount);
        }
    }
    int *Workspace = getArrayFromUser(muxCount);
    muxArray(Workspace,muxCount);

    // Be happy, don't 1
    return 0;
}

