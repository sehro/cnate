# Default to GCC
CC=gcc

all: build

build:
	$(CC) cnate.c -o cnate

clean:
	rm -f cnate
