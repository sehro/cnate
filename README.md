# CNATE
> I'm so sorry
-sehro

## Origin and description

Originally a python script called netapp-test-ext: I wanted to complete it so I did. Then I wanted it to go beyond what was absolutely necessary, so I could (on demand) demonstrate actual grasp of the principles involved, so I made nate.py. Now I've ported it to C. Don't ask me why, I really don't know.

## Requirements

- A C compiler

## Usage
```
$ make
$ ./cnate
```

## Credits

First of, my apologies to anyone actually building and running this dreck.

Second, Dennis M. Ritchie. The world didn't deserve you.

Last, but not least, Batman. I don't need a reason.
